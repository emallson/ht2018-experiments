import json
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.cm as cm
import networkx as nx

def load_trace(path):
    with open(path) as log:
        return [json.loads(line) for line in log if json.loads(line)["msg"] == "attempted insertion"]

def build_graph(trace):
    g = nx.DiGraph()
    for i, entry in enumerate(trace):
        g.add_node(entry['target'], weight=entry['weight'], success=entry['successful'], prob=entry['accept prob'], index=i)
        for pred in json.loads(entry['predecessors']):
            g.add_edge(pred, entry['target'])
    return g

def trace_layout(trace, g):
    layout = {}
    row = 0
    spread = 1
    for i, entry in enumerate(trace):
        x = i * spread
        preds = json.loads(entry['predecessors'])
        if len(preds) > 0:
            y = max([layout[pred][1] for pred in preds])
        else:
            y = row
            row -= 1
        layout[entry['target']] = (x, y)
    return layout

def trace_colors(trace, g, nodes=None, color_by='accept prob', cmapper=cm.get_cmap('Accent')):
    colors = {entry['target']: entry[color_by] for entry in trace}
    if nodes is None:
        nodes = g.nodes()
    return cmapper([colors[n] for n in g.nodes()])

def draw_nodes(g, ax, nodelist, pos, node_color, node_size, node_shape):
    xy = np.asarray([pos[v] for v in nodelist])
    node_collection = ax.scatter(xy[:, 0], xy[:, 1],
                                 s=node_size,
                                 c=node_color,
                                 marker=node_shape)

    node_collection.set_zorder(2)
    return node_collection
    
def draw_graph(fig, ax, trace, g, color_by='accept prob', color_with='Accent', edges=True, **kwargs):
    cmap = cm.get_cmap(color_with)
    layout = trace_layout(trace, g)
    succ = [node for node, data in g.nodes(data=True) if data['success']]
    fail = [node for node, data in g.nodes(data=True) if not data['success']]
    succ = draw_nodes(g, ax=ax, nodelist=succ, pos=layout, node_color=trace_colors(trace, g, succ, color_by, cmap), node_size=10, node_shape='o')   
    fail = draw_nodes(g, ax=ax, nodelist=fail, pos=layout, node_color=trace_colors(trace, g, fail, color_by, cmap), node_size=20, node_shape='x')
    if edges:
        nx.draw_networkx_edges(g, pos=layout, ax=ax, arrows=False, style='dotted', alpha=0.05)
    #  div = make_axes_locatable(ax)
    #  cax = div.append_axes('bottom', size='5%', pad=0.05)
    #  fig.colorbar(succ, cax=cax, orientation='horizontal', ticks=np.linspace(0,1,5, endpoint=True))
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    return fig, ax
