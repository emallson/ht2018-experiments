from .mixins import *
import luigi
from luigi.contrib.external_program import ExternalProgramTask

class ReconImpl(luigi.ExternalTask):
    bin = luigi.Parameter(default="recon")
    def output(self):
        return luigi.LocalTarget("./recon/target/release/{}".format(self.bin))

class Instance(HashOutputMixin, TempPathMixin, RustBacktraceMixin, ExternalProgramTask):
    LOG_PATH = "logs/inst/{}.bin"
    key = luigi.Parameter()
    graph = luigi.Parameter()
    weights = luigi.ChoiceParameter(choices=["scalar", "feature"])
    r = luigi.IntParameter(default=None)
    target = luigi.ChoiceParameter(choices=["untargeted", "targeted-bfs", "targeted-file"])
    target_count = luigi.Parameter(default=None) # legacy name -- this is just the parameter of the target; name not changed to preserve hash values

    def requires(self):
        return ReconImpl(bin="instance")

    def program_args(self):
        args = [
            self.input().path,
            self.graph,
            self._tmp_output,
        ]

        args += self.weight_args()
        args += self.target_args()

        return args

    def weight_args(self):
        if self.weights == "scalar":
            return ["--scalar-weights"]
        elif self.weights == "feature":
            return ["--features", self.r]

    def target_args(self):
        if self.target == "untargeted":
            return ["--untargeted"]
        elif self.target == "targeted-bfs":
            return ["--targeted-bfs", self.target_count]
        elif self.target == "targeted-file":
            return ["--targeted-file", self.target_count]


class Recon(HashOutputMixin, TempPathMixin, RustBacktraceMixin, ExternalProgramTask):
    LOG_PATH = "logs/recon/{}.log"
    key = luigi.Parameter()
    graph = luigi.Parameter()
    inst = TaskParameter(kind=Instance)
    k = luigi.IntParameter()
    accept = luigi.ChoiceParameter(choices=["etc", "zeta", "hmnm"])
    accept_param = luigi.FloatParameter(default=None)
    degree_incentive = luigi.BoolParameter(default=True)
    wi = luigi.BoolParameter(default=False)
    fof_scale = luigi.FloatParameter(default=0.5)

    def requires(self):
        return {"impl": ReconImpl(), "inst": self.inst}

    def program_args(self):
        args = [
            self.input()["impl"].path,
            self.graph,
            self.input()["inst"].path,
            self.k,
            self.flag(self.accept),
        ]

        if self.accept_param is not None:
            args += [self.accept_param]

        args += [
            "--log",
            self._tmp_output,
        ]

        if self.degree_incentive:
            args += ["--degree-incentive"]

        if self.wi:
            args += ["--wi"]

        args += ["--fof-scale", self.fof_scale]

        return args

    def flag(self, f):
        return "--{}".format(f)
