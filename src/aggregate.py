from .sbot import *
from . import trace
import luigi
import json
import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', size=14)
from matplotlib import pyplot as plt
import pandas as pd
from scipy import stats as st
import subprocess

def lookup_value(target, msg, key, map=None):
    with open(target.path, "r") as f:
        for line in f:
            obj = json.loads(line)
            if obj["msg"] == msg:
                if map is not None:
                    return map(obj[key])
                else:
                    return obj[key]

def lookup_values(target, msg, key, map=None, indexer=None):
    if indexer is None:
        results = []
    else:
        results = {}

    with open(target.path, "r") as f:
        for line in f:
            obj = json.loads(line)
            if obj["msg"] == msg:
                if map is not None:
                   item = map(obj[key])
                else:
                    item = obj[key]

                if indexer is None:
                    results += [item]
                else:
                    results[indexer(obj)] = item

    return results

class FeatureAnglesUniform(UncompletableMixin, DictOutputMixin, luigi.Task):
    LOG_PATH = "figs/angles.pdf"

    def requires(self):
        return {
            "scalar": Instance(
                key="uniform-test",
                graph="data/slashdot.bin",
                weights="scalar",
                target="untargeted",
            ),
            "feature": Instance(
                key="uniform-test",
                graph="data/slashdot.bin",
                weights="feature",
                r=10,
                target="untargeted"
            )
        }

    def run(self):
        def sample_weights(path, count):
            res = subprocess.run(["./recon-wsdm/target/release/instance", "sample-weights", path, str(count)], encoding='utf-8', check=True, stdout=subprocess.PIPE)
            return json.loads(res.stdout)
        from scipy.stats import mannwhitneyu
        from statsmodels.graphics.gofplots import qqplot_2samples
        scalar_weights = sample_weights(self.input()["scalar"].path, 10000)
        feature_weights = sample_weights(self.input()["feature"].path, 10000)
        print(np.mean(scalar_weights))
        print(np.mean(feature_weights))
        qqplot_2samples(np.array(scalar_weights), np.array(feature_weights),
                        line='45',
                        ylabel="Scalar Quantiles",
                        xlabel="Feature Quantiles").savefig(self.output().path, bbox_inches='tight')
        

class VaryingZeta(UncompletableMixin, DictOutputMixin, luigi.Task):
    repetitions = luigi.IntParameter()
    instances = luigi.IntParameter()
    graph = luigi.Parameter()
    ks = luigi.ListParameter(default=[10,25,50,75,100])
    targets = luigi.IntParameter(default=0)
    zetas = luigi.ListParameter(default=[0.0, 0.25, 0.5, 0.75, 1.0])
    degree_incentive = luigi.BoolParameter(default=False)

    LOG_PATH = "figs/zeta_{}.pdf"

    def requires(self):
        insts = [
            Instance(
                key=str(i),
                graph=self.graph,
                weights="feature",
                r=10,
                target="untargeted" if self.targets == 0 else "targeted-bfs",
                target_count=None if self.targets == 0 else self.targets,
            )
            for i in range(self.instances)
        ]

        return {
            z: {
                k: [
                    Recon(
                        key=str(i), 
                        graph=self.graph,
                        inst=inst,
                        k=k,
                        accept="zeta",
                        accept_param=z,
                        degree_incentive=self.degree_incentive,
                    )
                    for i in range(self.repetitions)
                    for inst in insts
                ]
                for k in self.ks
            }
            for z in self.zetas
        }
            

    def run(self):
        results = {z: {k: np.mean([lookup_value(task, "objective", "f", float) for task in tasks]) for k, tasks in ks.items()} for z, ks in self.input().items()}

        fig, ax = plt.subplots(1, tight_layout=True)
        for key in results.keys():
            items = list(results[key].items())
            items = sorted(items, key=lambda p: int(p[0]))
            xs = [p[0] for p in items]
            ys = [p[1] for p in items]
            ax.plot(xs, ys, label="$\\zeta = {}$".format(key))
        ax.legend(loc = 'best')
        ax.set_xlabel("$k$")
        ax.set_ylabel("Mean Total Benefit Gained")
        fig.savefig(self.output().path)

labels = {
    "wi": "AReST",
    "wsdm": "MINE",
    "bfs": "BFS",
    "comm": "Community",
    "un": "Untargeted",
    0.0: "Without FoF",
    0.5: "With FoF",
}

colors = {
    "wi": "C0",
    "wsdm": "C1",
    "bfs": "C0",
    "comm": "C1",
    "un": "C2",
    0.0: "C0",
    0.5: "C1",
}

markers = {
    "wi": "+",
    "wsdm": "^",
    "bfs": "+",
    "comm": "*",
    "un": "o",
    0.0: "+",
    0.5: "*",
}

class CompareWI(UncompletableMixin, DictOutputMixin, luigi.Task):
    repetitions = luigi.IntParameter()
    instances = luigi.IntParameter()
    graph = luigi.Parameter()
    ks = luigi.ListParameter(default=[10,25,50,75,100])
    targets = luigi.IntParameter(default=0)

    LOG_PATH = "figs/wi{{}}_{}.pdf"

    def requires(self):
        insts = [
            Instance(
                key=str(i),
                graph=self.graph,
                weights="scalar",
                target="untargeted" if self.targets == 0 else "targeted-bfs",
                target_count=None if self.targets == 0 else self.targets,
            )
            for i in range(self.instances)
        ]

        return {
            alg: {
                k: [
                    Recon(
                        key=str(i), 
                        graph=self.graph,
                        inst=inst,
                        k=k,
                        accept="etc",
                        wi=alg=="wi"
                    )
                    for i in range(self.repetitions)
                    for inst in insts
                ]
                for k in self.ks
            }
            for alg in ["wi", "wsdm"]
        }
            
    def output(self, omit=set()):
        base = super().output(omit)
        return {
            "cmp": luigi.LocalTarget(base.path.format("")),
            "breakdown": luigi.LocalTarget(base.path.format("-breakdown"))
        }

    def run(self):
        results = {z: {k: np.mean([lookup_value(task, "objective", "f", float) for task in tasks]) for k, tasks in ks.items()} for z, ks in self.input().items()}
        breakdown = {z: np.array([[k] + [lookup_value(task, "objective", kind, float) for kind in ("friend", "edge", "fof")] for k, tasks in ks.items() for task in tasks]) for z, ks in self.input().items()}

        fig, ax = plt.subplots(1, tight_layout=True, figsize=(4,3))
        for key in results.keys():
            items = list(results[key].items())
            items = sorted(items, key=lambda p: int(p[0]))
            xs = [p[0] for p in items]
            ys = [p[1] for p in items]
            ax.plot(xs, ys, label=labels[key])
        ax.legend(loc = 'best')
        ax.set_xlabel("$k$")
        ax.set_ylabel("Mean Total Benefit Gained")
        fig.savefig(self.output()["cmp"].path)

        fig, axs = plt.subplots(2, tight_layout=True, figsize=(4,3))
        fig.subplots_adjust(hspace=0, wspace=0)
        for i, key in enumerate(breakdown.keys()):
            df = pd.DataFrame(breakdown[key], columns=["k", "friend", "edge", "fof"])
            means = df.groupby("k").mean()
            ax = axs[i]
            ax.plot(means.index.values, means["friend"].values, label="$B_f$")
            ax.plot(means.index.values, means["edge"].values + means["friend"].values, label="$B_e$")
            ax.plot(means.index.values, means["fof"].values + means["edge"].values + means["friend"].values, label="$B_{fof}$")

            if i == 1:
                ax.legend(loc='lower left', ncol=3, fontsize=12, bbox_to_anchor=(0,-1,1,0.2))
            ax.set_title(labels[key], fontsize=12)
            #  ax.set_ylabel(labels[key])
        fig.savefig(self.output()["breakdown"].path, bbox_inches="tight")


class CompareWITrace(UncompletableMixin, DictOutputMixin, luigi.Task):
    repetitions = luigi.IntParameter()
    graph = luigi.Parameter()
    k = luigi.IntParameter()
    targets = luigi.IntParameter()
    inst_key= luigi.Parameter()

    LOG_PATH = "figs/wi-trace_{{}}_{{}}_{}.pdf"

    def requires(self):
        inst = Instance(
            key=self.inst_key,
            graph=self.graph,
            weights="scalar",
            target="untargeted" if self.targets == 0 else "targeted-bfs",
            target_count=None if self.targets == 0 else self.targets,
        )

        return {
            alg: [
                Recon(
                    key=str(i), 
                    graph=self.graph,
                    inst=inst,
                    k=self.k,
                    accept="etc",
                    wi=alg=="wi"
                )
                for i in range(self.repetitions)
            ]
            for alg in ["wi", "wsdm"]
        }

    def output(self, omit=set()):
        base = super().output(omit=omit)
        return {alg: luigi.LocalTarget(base.path.format(alg, len(reps)-1)) for alg, reps in self.input().items()}

    def run(self):
        for alg, reps in self.input().items():
            fig, axa = plt.subplots(len(reps), figsize=(4, 1.5 * len(reps)))
            for i, rep in enumerate(reps):
                tr = trace.load_trace(rep.path)
                g = trace.build_graph(tr)
                trace.draw_graph(fig, axa[i], tr, g, color_by='weight')

            fig.savefig(self.output()[alg].path, bbox_inches="tight")

class CompareBenTrace(UncompletableMixin, DictOutputMixin, luigi.Task):
    repetitions = luigi.IntParameter()
    graph = luigi.Parameter()
    k = luigi.IntParameter()
    targets = luigi.IntParameter()
    inst_key= luigi.Parameter()

    LOG_PATH = "figs/fof-trace_{{}}_{{}}_{}.pdf"

    def requires(self):
        inst = Instance(
            key=self.inst_key,
            graph=self.graph,
            weights="scalar",
            target="untargeted" if self.targets == 0 else "targeted-bfs",
            target_count=None if self.targets == 0 else self.targets,
        )

        return {
            fof: [
                Recon(
                    key=str(i), 
                    graph=self.graph,
                    inst=inst,
                    k=self.k,
                    accept="etc",
                    wi=False,
                    fof_scale=fof
                )
                for i in range(self.repetitions)
            ]
            for fof in [0.0, 0.5]
        }

    def run(self):
        for fof, reps in self.input().items():
            fig, axa = plt.subplots(len(reps), figsize=(4,len(reps)))
            for i, rep in enumerate(reps):
                tr = trace.load_trace(rep.path)
                g = trace.build_graph(tr)
                trace.draw_graph(fig, axa[i], tr, g, color_by='weight')

            fig.savefig(self.output().path.format(fof, i), bbox_inches="tight")

class CompareTargetSettings(UncompletableMixin, DictOutputMixin, luigi.Task):
    repetitions = luigi.IntParameter()
    instances = luigi.IntParameter()
    #  graph = luigi.Parameter(default="dblp.bin")
    ks = luigi.ListParameter(default=[10,25,50,75,100])
    bfs_targets = luigi.IntParameter(default=150)
    fof_scale = luigi.FloatParameter(default=0.5)
    # manually chosen communities based on size (between 100 and 200
    # elements each)
    communities = luigi.ListParameter(default=[44,45,46,47,48,49,50,51,52,53])

    LOG_PATH = "figs/targets{{}}_{}.pdf"

    graph = "data/dblp.bin"

    def requires(self):
        bfs_insts = [
            Instance(
                key=str(i),
                graph=self.graph,
                weights="scalar",
                target="targeted-bfs",
                target_count=self.bfs_targets,
            )
            for i in range(self.instances)
        ]

        comm_insts = [
            Instance(
                key=str(c),
                graph=self.graph,
                weights="scalar",
                target="targeted-file",
                target_count="data/communities/dblp.bin/{}.txt".format(c),
            )
            for c in self.communities
        ]

        un_insts = [
            Instance(
                key=str(i),
                graph=self.graph,
                weights="scalar",
                target="untargeted",
            )
            for i in range(self.instances)
        ]

        return {
            key: {
                k: [
                    Recon(
                        key=str(i), 
                        graph=self.graph,
                        inst=inst,
                        k=k,
                        accept="etc",
                        fof_scale=self.fof_scale,
                        wi=False
                    )
                    for i in range(self.repetitions)
                    for inst in insts
                ]
                for k in self.ks
            }
            for key, insts in [("bfs", bfs_insts), ("comm", comm_insts), ("un", un_insts)]
        }
            
    def output(self, omit=set()):
        base = super().output(omit)
        return {
            "benefit": luigi.LocalTarget(base.path.format("-benefit")),
            "friend": luigi.LocalTarget(base.path.format("-friend")),
            "accept": luigi.LocalTarget(base.path.format("-accept")),
        }

    def run(self):
        from functools import lru_cache
        @lru_cache(maxsize=128)
        def total_weight(path):
            res = subprocess.run(["./recon-wsdm/target/release/instance", "total-weight", self.graph, path], encoding='utf-8', check=True, stdout=subprocess.PIPE)
            return float(res.stdout)
        #  results = {z: {k: np.mean([lookup_value(task.output(), "objective", "f", float) / total_weight(task.inst.output().path) for task in tasks]) for k, tasks in ks.items()} for z, ks in self.requires().items()}
        #  breakdown = {z: np.array([[k] + [lookup_value(task, "objective", kind, float) for kind in ("friend", "edge", "fof")] for k, tasks in ks.items() for task in tasks]) for z, ks in self.input().items()}

        #  fig, ax = plt.subplots(1, tight_layout=True, figsize=(4,3))
        #  for key in results.keys():
            #  items = list(results[key].items())
            #  items = sorted(items, key=lambda p: int(p[0]))
            #  xs = [p[0] for p in items]
            #  ys = [p[1] for p in items]
            #  ax.plot(xs, ys, label=labels[key])
        #  ax.legend(loc = 'best')
        #  ax.set_xlabel("$k$")
        #  ax.set_ylabel("Mean Total Benefit Gained")
        #  fig.savefig(self.output()["cmp"].path)

        #  fig, axs = plt.subplots(2, tight_layout=True, figsize=(4,3))
        #  fig.subplots_adjust(hspace=0, wspace=0)
        #  for i, key in enumerate(breakdown.keys()):
            #  df = pd.DataFrame(breakdown[key], columns=["k", "friend", "edge", "fof"])
            #  means = df.groupby("k").mean()
            #  ax = axs[i]
            #  ax.plot(means.index.values, means["friend"].values, label="$B_f$")
            #  ax.plot(means.index.values, means["edge"].values + means["friend"].values, label="$B_e$")
            #  ax.plot(means.index.values, means["fof"].values + means["edge"].values + means["friend"].values, label="$B_{fof}$")

            #  if i == 1:
                #  ax.legend(loc='lower left', ncol=3, fontsize=12, bbox_to_anchor=(0,-1,1,0.2))
            #  ax.set_title(labels[key], fontsize=12)
            #  #  ax.set_ylabel(labels[key])
        #  fig.savefig(self.output()["breakdown"].path, bbox_inches="tight")
        #  results = {z: {k: np.ndarray([lookup_values(task.output(), "current benefit", "total", float, np.ndarray) / total_weight(task.inst.output().path) for task in tasks]) for k, tasks in ks.items()} for z, ks in self.requires().items()}

        results = {z: np.mean(np.array([np.array(lookup_values(task.output(), "current benefit", "friend", float)[::25]) / lookup_value(task.output(), "loaded graph", "targets", float) for task in ks[self.ks[-1]]]), axis=0)
                   for z, ks in self.requires().items()}

        fig, ax = plt.subplots(1, tight_layout=True, figsize=(4,3))
        for key, vals in results.items():
            ax.plot(np.arange(1,301,25), vals, label=labels[key], color=colors[key], marker=markers[key])
        ax.legend(loc='best')
        ax.set_xlabel("\\# Friend Requests")
        ax.set_ylabel("\\% Targets Befriended")
        ax.set_ylim([0,0.2])

        fig.savefig(self.output()["friend"].path)

        accepts = {z: np.clip(np.mean(np.array([np.array(lookup_values(task.output(), "attempted insertion", "accept prob", float)[::25]) for task in ks[self.ks[-1]]]), axis=0), 0.0, 1.0)
                   for z, ks in self.requires().items()}
        fig, ax = plt.subplots(1, tight_layout=True, figsize=(4,3))
        for key, vals in accepts.items():
            if key != "un":
                ax.plot(np.arange(1,301,25), vals, label=labels[key], color=colors[key], marker=markers[key])
        ax.legend(loc='best')
        ax.set_xlabel("\\# Friend Requests")
        ax.set_ylabel("$\\alpha$")
        ax.set_ylim([0.0,1.0])

        fig.savefig(self.output()["accept"].path)

        benefits = {z: np.mean(np.array([np.array(lookup_values(task.output(), "current benefit", "total", float)[::25]) for task in ks[self.ks[-1]]]), axis=0)
                   for z, ks in self.requires().items()}
        fig, ax = plt.subplots(1, tight_layout=True, figsize=(4,3))
        for key, vals in benefits.items():
            if key != "un":
                ax.plot(np.arange(1,301,25), vals, label=labels[key], color=colors[key], marker=markers[key])
        ax.legend(loc='best')
        ax.set_xlabel("\\# Friend Requests")
        ax.set_ylabel("Mean Total Benefit")
        #  ax.set_ylim([0.0,1.0])

        fig.savefig(self.output()["benefit"].path)

class CompareTargetSettingsTrace(UncompletableMixin, DictOutputMixin, luigi.Task):
    repetitions = luigi.IntParameter()
    graph = luigi.Parameter()
    k = luigi.IntParameter()
    show_first = luigi.IntParameter(default=None)
    bfs_targets = luigi.IntParameter(default=150)
    community = luigi.IntParameter()
    bfs_key = luigi.Parameter()
    fof_scale = luigi.FloatParameter(default=0.5)

    LOG_PATH = "figs/targets-trace_{{}}_{{}}_{}.pdf"

    def requires(self):
        bfs_insts = Instance(
            key=self.bfs_key,
            graph=self.graph,
            weights="scalar",
            target="targeted-bfs",
            target_count=self.bfs_targets,
        )


        comm_insts = Instance(
            key=str(self.community),
            graph=self.graph,
            weights="scalar",
            target="targeted-file",
            target_count="data/communities/dblp.bin/{}.txt".format(self.community),
        )

        return {
            set: [
                Recon(
                    key=str(i), 
                    graph=self.graph,
                    inst=inst,
                    k=self.k,
                    accept="etc",
                    wi=False,
                    fof_scale=self.fof_scale,
                )
                for i in range(self.repetitions)
            ]
            for set, inst in [("bfs", bfs_insts), ("comm", comm_insts)]
        }

    def output(self, omit=set()):
        base = super().output(omit=omit)
        return {alg: luigi.LocalTarget(base.path.format(alg, len(reps)-1)) for alg, reps in self.input().items()}

    def run(self):
        for alg, reps in self.input().items():
            els = self.k if self.show_first is None else self.show_first
            fig, axa = plt.subplots(len(reps), figsize=(els / 50 * 4, 1.5 * len(reps)))
            for i, rep in enumerate(reps):
                tr = trace.load_trace(rep.path)
                if self.show_first is not None:
                    tr = tr[:self.show_first]
                g = trace.build_graph(tr)
                trace.draw_graph(fig, axa[i], tr, g, color_by='weight', color_with='Accent', edges=True)

            fig.savefig(self.output()[alg].path, bbox_inches="tight")

class CompareFoFScale(UncompletableMixin, DictOutputMixin, luigi.Task):
    repetitions = luigi.IntParameter()
    instances = luigi.IntParameter()
    #  graph = luigi.Parameter(default="dblp.bin")
    k = luigi.IntParameter()
    bfs_targets = luigi.IntParameter(default=150)
    fof_scales = luigi.ListParameter(default=[0.0, 0.5])
    # manually chosen communities based on size (between 100 and 200
    # elements each)
    communities = luigi.ListParameter(default=[44,45,46,47,48,49,50,51,52,53])

    LOG_PATH = "figs/fofs{{}}_{}.pdf"

    graph = "data/dblp.bin"

    def requires(self):
        bfs_insts = [
            Instance(
                key=str(i),
                graph=self.graph,
                weights="scalar",
                target="targeted-bfs",
                target_count=self.bfs_targets,
            )
            for i in range(self.instances)
        ]

        comm_insts = [
            Instance(
                key=str(c),
                graph=self.graph,
                weights="scalar",
                target="targeted-file",
                target_count="data/communities/dblp.bin/{}.txt".format(c),
            )
            for c in self.communities
        ]

        un_insts = [
            Instance(
                key=str(i),
                graph=self.graph,
                weights="scalar",
                target="untargeted",
            )
            for i in range(self.instances)
        ]

        return {
            key: {
                f: [
                    Recon(
                        key=str(i), 
                        graph=self.graph,
                        inst=inst,
                        k=self.k,
                        accept="etc",
                        fof_scale=f,
                        wi=False
                    )
                    for i in range(self.repetitions)
                    for inst in insts
                ]
                for f in self.fof_scales
            }
            for key, insts in [("bfs", bfs_insts), ("comm", comm_insts), ("un", un_insts)]
        }
            
    def output(self, omit=set()):
        base = super().output(omit)
        return {
            "benefit": luigi.LocalTarget(base.path.format("-benefit")),
            "cumaccept": luigi.LocalTarget(base.path.format("-cumaccept"))
        }

    def run(self):
        benefits = {k: {key: np.mean(np.array([np.array(lookup_values(task.output(), "current benefit", "total", float)[::25]) for task in tasks]), axis=0)
                     for key, tasks in vs.items()}
                    for k, vs in self.requires().items()}
        fig, ax = plt.subplots(1, tight_layout=True, figsize=(4,3))
        for key, vals in benefits["comm"].items():
            if key != "un":
                ax.plot(np.arange(1,301,25), vals, label=labels[key], color=colors[key], marker=markers[key])
        ax.legend(loc='best')
        ax.set_xlabel("\\# Friend Requests")
        ax.set_ylabel("Mean Total Benefit")
        #  ax.set_ylim([0.0,1.0])

        fig.savefig(self.output()["benefit"].path)

        cumaccept = {k: {key: np.mean((np.cumsum(np.array([np.array(lookup_values(task.output(), "attempted insertion", "successful", bool)) for task in tasks]), axis=1) / np.arange(1, 301)), axis=0)
                     for key, tasks in vs.items()}
                    for k, vs in self.requires().items()}
        fig, ax = plt.subplots(1, tight_layout=True, figsize=(4,3))
        for key, vals in cumaccept["comm"].items():
            ax.plot(np.arange(1,301,25), vals[::25], label=labels[key], color=colors[key], marker=markers[key])
        ax.legend(loc='best')
        ax.set_xlabel("\\# Friend Requests")
        ax.set_ylabel("Cumulative Acceptance Rate")
        ax.set_ylim([0.0,1.0])

        fig.savefig(self.output()["cumaccept"].path)

class Everything(luigi.WrapperTask):
    """This task is **expicitly parameterless** and is used to generate all figures used in the paper.
    
    Note that since problem instances are still randomly generated, any *Trace figures are unlikely to come out identical to those in the paper.
    """
    def requires(self):
        # fig 1
        yield CompareWI(repetitions=25, instances=10, graph="data/slashdot.bin", ks=[10,25,50,75,100], targets=100, should_complete=True)
        # fig 2
        yield CompareWITrace(repetitions=2, graph="data/slashdot.bin", k=50, targets=100, inst_key=1, should_complete=True)
        # fig 3a/4a
        yield CompareTargetSettings(repetitions=25, instances=10, ks=[300], bfs_targets=100, fof_scale=0.0, should_complete=True)
        # fig 3b/4b
        yield CompareTargetSettings(repetitions=25, instances=10, ks=[300], bfs_targets=100, fof_scale=0.5, should_complete=True)
        # fig 5a
        yield CompareTargetSettingsTrace(repetitions=2, k=300, show_first=50, fof_scale=0.0, bfs_key=1, bfs_targets=100, community=44, graph="data/dblp.bin", should_complete=True)
        # fig 5b
        yield CompareTargetSettingsTrace(repetitions=2, k=300, show_first=50, fof_scale=0.5, bfs_key=1, bfs_targets=100, community=44, graph="data/dblp.bin", should_complete=True)
        # fig 6/7
        yield CompareFoFScale(bfs_targets=100, fof_scales=[0.0, 0.5], instances=10, k=300, repetitions=25)
