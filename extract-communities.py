#!/usr/bin/env python3
"""
Convert the SNAP community list format to a set of files corresponding to the communities.

Usage:
    ./extract-communities.py <snap-input> <out-dir>
    ./extract-communities.py (-h | --help)

Options:
    -h --help           Show this screen.
"""
from docopt import docopt
import os
import os.path as pt

if __name__ == "__main__":
    args = docopt(__doc__)

    with open(args["<snap-input>"]) as snap:
        os.makedirs(args["<out-dir>"], exist_ok=True)
        for i, comm in enumerate(snap):
            with open(pt.join(args["<out-dir>"], "{}.txt".format(i)), "w") as out:
                out.write('\n'.join(comm.split()))
